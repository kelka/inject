<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Inject</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<script type="text/javascript" src="jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="jquery.slidertron-1.3.js"></script>
<link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:200,300,400,600,700,900" rel="stylesheet" />
<link href="default.css" rel="stylesheet" type="text/css" media="all" />
<link href="fonts.css" rel="stylesheet" type="text/css" media="all" />

<!--[if IE 6]><link href="default_ie6.css" rel="stylesheet" type="text/css" /><![endif]-->

</head>
<body>
<div id="header-wrapper">
    <div id="logo">
  <div id="header" class="container">
	  <h1><a href="#">:> INJECT</a></h1>
	  </div>
  <div id="copyright" class="container">
	  </div>
    <div id="menu">
      <ul>
        <li class="current_page_item"><a href="index.php?menu=acceuil" accesskey="1" title="">Homepage</a></li>
        <li><a href="index.php?menu=about" accesskey="3" title="">About Us</a></li>
        <li><a href="index.php?menu=chat" accesskey="4" title="">Chat</a></li>
		 <li><a href="index.php?menu=login" title="">Login</a></li>
      </ul>
    </div>
  </div>
</div>
<div id="header-featured"> </div>
<?PHP
   include("scripts/modules.php");
?>
<div id="copyright" class="container">
  <p>Copyright (c) 2014 Sitename.com. All rights reserved. | Photos by <a href="http://fotogrph.com/">Fotogrph</a> | Design by <a href="http://www.freecsstemplates.org/" rel="nofollow">FreeCSSTemplates.org</a>.</p>
</div>
</body>
</html>
